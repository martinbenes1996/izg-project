# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/martin/izg-project/examples/triangleExample.c" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/examples/triangleExample.c.o"
  "/home/martin/izg-project/student/bunny.c" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/student/bunny.c.o"
  "/home/martin/izg-project/student/camera.c" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/student/camera.c.o"
  "/home/martin/izg-project/student/globals.c" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/student/globals.c.o"
  "/home/martin/izg-project/student/linearAlgebra.c" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/student/linearAlgebra.c.o"
  "/home/martin/izg-project/student/main.c" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/student/main.c.o"
  "/home/martin/izg-project/student/mouseCamera.c" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/student/mouseCamera.c.o"
  "/home/martin/izg-project/student/student_cpu.c" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/student/student_cpu.c.o"
  "/home/martin/izg-project/student/student_pipeline.c" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/student/student_pipeline.c.o"
  "/home/martin/izg-project/student/student_shader.c" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/student/student_shader.c.o"
  "/home/martin/izg-project/student/swapBuffers.c" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/student/swapBuffers.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/martin/izg-project/gpu/gpu.cpp" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/gpu/gpu.cpp.o"
  "/home/martin/izg-project/tests/conformanceTests.cpp" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/tests/conformanceTests.cpp.o"
  "/home/martin/izg-project/tests/performanceTest.cpp" "/home/martin/izg-project/build/CMakeFiles/izgProjekt2017.dir/tests/performanceTest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
